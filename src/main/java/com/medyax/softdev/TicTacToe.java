/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medyax.softdev;

import java.util.Scanner;

public class TicTacToe {

    private static void showWelcome() {
        System.out.println("Welcome to OX game!");
    }

    private static void showTable(char[][] table) {
        System.out.println(" 123");

        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    private static void showTurn(char player) {
        System.out.println(player + " turn.");
    }

    private static boolean checkColumn(char[][] table, char player, int col) {
        for (int row = 0; row < table.length; row++) {
            if (table[row][col] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkRow(char[][] table, char player, int row) {
        for (int col = 0; col < table.length; col++) {
            if (table[row][col] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkTayang(char[][] table, char player) {
        if ((table[0][0] == player)
                && (table[1][1] == player)
                && (table[2][2] == player)) {
            return true;
        } else if ((table[0][2] == player)
                && (table[1][1] == player)
                && (table[2][0] == player)) {
            return true;
        }
        return false;
    }

    private static boolean checkWin(char[][] table, char player,
            int row, int col) {

        if (checkColumn(table, player, col)
                || checkRow(table, player, row)
                || checkColumn(table, player, col)
                || checkTayang(table, player)) {
            return true;
        }
        return false;

    }

    private static boolean inputAndCheckWin(char[][] table, char player) {
        Scanner sc = new Scanner(System.in);
        int row, col;

        while (true) {
            System.out.println("Please unput Row Col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("That space isn't empty!");
        }
        //Check winner.
        if (checkWin(table, player, row, col)) {
            return true;
        }
        return false;
    }

    private static void showWinner(char player) {
    }

    private static void showResult(char winner) {
        if (winner == 'X') {
            System.out.println("X win!");
        } else if (winner == 'O') {
            System.out.println("O win!");
        } else {
            System.out.println("DRAW!");
        }
    }

    private static char switchPlayer(char player) {
        if (player == 'X') {
            return 'O';
        } else {
            return 'X';
        }
    }

    private static void showBye() {
        System.out.println("Bye bye!");
    }

    public static void main(String[] args) {

        char[][] table = {
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'},};

        char player = 'X'; //Who goes first.
        char winner = '-';

        int round = 0;

        //Classes
        showWelcome();

        do {
            showTable(table);
            showTurn(player);

            //Input & Check winner...
            if (inputAndCheckWin(table, player)) {
                
                winner = player;
                break;
            }
            //Swith player...
            round++;
            player = switchPlayer(player);
        } while (round < 9);
        
        showTable(table);
        showResult(winner);
        showBye();
    }
}
